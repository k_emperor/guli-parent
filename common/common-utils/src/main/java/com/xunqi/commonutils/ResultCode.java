package com.xunqi.commonutils;

/**
 * @Description: 统一状态码
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-26 19:54
 **/

public interface ResultCode {

    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;

}
