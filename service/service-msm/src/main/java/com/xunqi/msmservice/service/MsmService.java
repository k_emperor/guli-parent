package com.xunqi.msmservice.service;

import java.util.Map;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-08 08:55
 **/
public interface MsmService {

    /**
     * 发送短信验证码
     * @param param
     * @param phone
     * @return
     */
    boolean sendMsm(Map<String, Object> param, String phone);
}
