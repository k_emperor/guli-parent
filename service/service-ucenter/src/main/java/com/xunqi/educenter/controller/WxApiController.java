package com.xunqi.educenter.controller;

import com.google.gson.Gson;
import com.xunqi.commonutils.JwtUtils;
import com.xunqi.educenter.entity.UcenterMember;
import com.xunqi.educenter.service.UcenterMemberService;
import com.xunqi.educenter.utils.ConstantWxUtils;
import com.xunqi.educenter.utils.HttpClientUtils;
import com.xunqi.servicebase.exception.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-10 14:14
 **/

@CrossOrigin
@Controller
@RequestMapping(value = "/api/ucenter/wx")
public class WxApiController {


    @Autowired
    private UcenterMemberService memberService;

    /**
     * 获取扫码人的信息，添加数据
     * @return
     */
    @GetMapping(value = "/callback")
    public String callback(String code,String state) throws Exception {

       try {
           //得到授权临时票据code
           System.out.println(code);
           System.out.println(state);
           //从redis中将state获取出来，和当前传入的state作比较
           //如果一致则放行，如果不一致则抛出异常：非法访问

           //向认证服务器发送请求换取access_token
           //2、拿着code请求 微信固定的地址，得到两个 access_token 和 openid
           String baseAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                   "?appid=%s" +
                   "&secret=%s" +
                   "&code=%s" +
                   "&grant_type=authorization_code";

           //拼接三个参数：id 秘钥 和 code 值
           String accessTokenUrl = String.format(
                   baseAccessTokenUrl,
                   ConstantWxUtils.WX_OPEN_APP_ID,
                   ConstantWxUtils.WX_OPEN_APP_SECRET,
                   code
           );

           //使用httpClient发送请求，得到返回结果

           String accessTokenInfo = null;
           try {
               accessTokenInfo = HttpClientUtils.get(accessTokenUrl);
               System.out.println("accessToken=============" + accessTokenInfo);
           } catch (Exception e) {
               throw new GuliException(20001, "获取access_token失败");
           }

           //从accessTokenInfo中获取出来两个值 access_token 和 oppenid
           //把accessTokenInfo字符串转换成map集合，根据map里面中的key取出相对应的value
           Gson gson = new Gson();
           HashMap accessMap = gson.fromJson(accessTokenInfo, HashMap.class);
           String accessToken = (String) accessMap.get("access_token");
           String openid = (String) accessMap.get("openid");

           //3、拿到access_token 和 oppenid，再去请求微信提供固定的API，获取到扫码人的信息
           //TODO 查询数据库当前用用户是否曾经使用过微信登录
           UcenterMember member = this.memberService.getByOpenid(openid);
           if (member == null) {
               System.out.println("新用户注册");
               //访问微信的资源服务器，获取用户信息
               String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                       "?access_token=%s" +
                       "&openid=%s";
               String userInfoUrl = String.format(baseUserInfoUrl, accessToken, openid);
               //发送请求
               String resultUserInfo = null;
               try {
                   resultUserInfo = HttpClientUtils.get(userInfoUrl);
                   System.out.println("resultUserInfo==========" + resultUserInfo);
               } catch (Exception e) {
                   throw new GuliException(20001, "获取用户信息失败");
               }

               //解析json
               HashMap userInfoMap = gson.fromJson(resultUserInfo, HashMap.class);
               String nickName = (String) userInfoMap.get("nickname");      //昵称
               Double sex = (Double) userInfoMap.get("sex");        //性别
               String headimgurl = (String) userInfoMap.get("headimgurl");      //微信头像

               //把扫码人的信息添加到数据库中
               member = new UcenterMember();
               member.setAvatar(headimgurl);
               member.setOpenid(openid);
               member.setNickname(nickName);
               member.setIsDisabled(false);
               member.setSex(Integer.valueOf(Double.valueOf(sex).intValue()));
               this.memberService.save(member);
           }

           //使用jwt根据member对象生成token字符串
           //最后返回首页面，通过路径传递出去
           String jwtToken = JwtUtils.getJwtToken(member.getId(), member.getNickname());

           //最后：返回首页面
           return "redirect:http://localhost:3000?token=" + jwtToken;

       } catch (Exception e) {
           throw new GuliException(20001,e.getMessage());
       }
    }



    /**
     * 生成微信扫描二维码
     * @return
     * @throws UnsupportedEncodingException
     */
    @GetMapping(value = "/login")
    public String getWxCode() throws UnsupportedEncodingException {

        //微信开发平台授权baseUrl   %s相当于？表示占位符
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";

        //对redirect_url进行URLEncoder编码
        String redirect_url = ConstantWxUtils.WX_OPEN_REDIRECT_URL;
        redirect_url = URLEncoder.encode(redirect_url,"UTF-8");

        // 防止csrf攻击（跨站请求伪造攻击）
        //String state = UUID.randomUUID().toString().replaceAll("-", "");//一般情况下会使用一个随机数
        String state = "hjl.mynatapp.cc";//为了让大家能够使用我搭建的外网的微信回调跳转服务器，这里填写你在ngrok的前置域名
        System.out.println("state = " + state);
        // 采用redis等进行缓存state 使用sessionId为key 30分钟后过期，可配置
        //键： "wechar-open-state-" + httpServletRequest.getSession().getId()
        //值： satte
        //过期时间： 30分钟
        //生成qrcodeUrl

        //设置%s中的值
        String url = String.format(
                    baseUrl,
                    ConstantWxUtils.WX_OPEN_APP_ID,
                    redirect_url,
                    "xunqi"
        );

        //重定向到请求微信地址
        return "redirect:" + url;
    }

}
