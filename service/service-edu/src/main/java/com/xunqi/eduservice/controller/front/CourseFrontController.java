package com.xunqi.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.commonutils.JwtUtils;
import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduCourse;
import com.xunqi.eduservice.entity.chapter.ChapterVo;
import com.xunqi.eduservice.feign.OrderFeign;
import com.xunqi.eduservice.service.EduChapterService;
import com.xunqi.eduservice.service.EduCourseService;
import com.xunqi.eduservice.vo.CourseFrontVo;
import com.xunqi.eduservice.vo.CourseWebVo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-11 15:08
 **/

@RestController
@RequestMapping(value = "/eduservice/coursefront")
public class CourseFrontController {


    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrderFeign orderFeign;

    /**
     * 条件查询带分页查询课程
     * @param page
     * @param limit
     * @param courseFrontVo
     * @return
     */
    @PostMapping(value = "/getFrontCourseList/{page}/{limit}")
    public R getFrontCourseList(@PathVariable("page") Integer page,
                                @PathVariable("limit") Integer limit,
                                @RequestBody(required = false) CourseFrontVo courseFrontVo) {

        Page<EduCourse> coursePage = new Page<>(page,limit);

        Map<String,Object> map = courseService.getFrontCourseList(coursePage,courseFrontVo);

        return R.ok().data(map);
    }


    /**
     * 根据课程id查询课程详细信息
     * @param courseId
     * @return
     */
    @GetMapping(value = "/getFrontCourseInfo/{courseId}")
    public R getFrontCourseInfo(@PathVariable("courseId") String courseId, HttpServletRequest request) {

        //根据课程id查询课程详细信息
        CourseWebVo courseWebVo = courseService.getBaseCourseInfo(courseId);

        //根据课程id查询章节和小节信息
        List<ChapterVo> chapterVoList = chapterService.getChapterVideo(courseId);

        //根据课程id和用户id查询当前课程是否已被支付过了
        boolean buyCourse = false;
        String memberIdByJwtToken = JwtUtils.getMemberIdByJwtToken(request);
        if (!StringUtils.isEmpty(memberIdByJwtToken)) {
            buyCourse = orderFeign.isBuyCourse(courseId, memberIdByJwtToken);
        }

        return R.ok().data("courseWebVo",courseWebVo).data("chapterVoList",chapterVoList).data("isBuy",buyCourse);
    }

    @ApiOperation(value = "根据课程id查询课程信息",notes = "根据课程id查询课程信息",httpMethod = "GET")
    @GetMapping(value = "/getCourseInfo")
    public CourseWebVo getCourseInfo(@ApiParam(name = "courseId",value = "课程信息id",required = true)
                           @RequestParam("courseId") String courseId) {

        CourseWebVo baseCourseInfo = this.courseService.getBaseCourseInfo(courseId);

        return baseCourseInfo;
    }


}
