package com.xunqi.eduservice.feign.impl;

import com.xunqi.eduservice.feign.UcenterFeign;
import com.xunqi.eduservice.vo.UcenterMemberVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 11:30
 **/

@Slf4j
@Component
public class UcenterFeignImpl implements UcenterFeign {

    @Override
    public UcenterMemberVo getByTokenUcenterInfo(String id) {

        log.info("用户未登录......");

        return null;
    }
}
