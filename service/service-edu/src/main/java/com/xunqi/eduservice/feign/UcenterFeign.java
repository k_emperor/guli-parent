package com.xunqi.eduservice.feign;

import com.xunqi.eduservice.feign.impl.UcenterFeignImpl;
import com.xunqi.eduservice.vo.UcenterMemberVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 11:28
 **/

@FeignClient(value = "service-ucenter",fallback = UcenterFeignImpl.class)
@Component
public interface UcenterFeign {

    @GetMapping(value = "/educenter/member/getByTokenUcenterInfo")
    UcenterMemberVo getByTokenUcenterInfo(@RequestParam("jwtToken") String jwtToken);

}
