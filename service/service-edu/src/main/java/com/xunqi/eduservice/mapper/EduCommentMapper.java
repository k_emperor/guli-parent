package com.xunqi.eduservice.mapper;

import com.xunqi.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
