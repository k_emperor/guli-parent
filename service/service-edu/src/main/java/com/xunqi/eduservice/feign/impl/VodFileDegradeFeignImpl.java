package com.xunqi.eduservice.feign.impl;

import com.xunqi.commonutils.R;
import com.xunqi.eduservice.feign.VodFeign;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-06 10:21
 **/

@Component
public class VodFileDegradeFeignImpl implements VodFeign {

    @Override
    public R removeAliyVideo(String id) {
        return R.error().message("连接超时，请稍后再试！！！");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        return R.error().message("连接超时，请稍后再试！！！");
    }
}
