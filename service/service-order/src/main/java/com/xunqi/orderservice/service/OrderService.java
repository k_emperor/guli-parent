package com.xunqi.orderservice.service;

import com.xunqi.orderservice.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
public interface OrderService extends IService<Order> {

    /**
     * 创建订单，返回订单号
     * @param courseId
     * @param request
     * @return
     */
    String createOrderInfo(String courseId, HttpServletRequest request);

    /**
     * 根据课程id和用户id查询订单表中订单的状态
     * @param courseId
     * @param memberId
     * @return
     */
    boolean isBuyCourse(String courseId, String memberId);
}
