package com.xunqi.orderservice.feign.impl;

import com.xunqi.orderservice.feign.UcenterFeign;
import com.xunqi.orderservice.vo.UcenterMemberVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 15:27
 **/

@Slf4j
@Component
public class UcenterFeignImpl implements UcenterFeign {

    @Override
    public UcenterMemberVo getByTokenUcenterInfo(String jwtToken) {
        log.error("用户信息不存在...");
        return null;
    }
}
