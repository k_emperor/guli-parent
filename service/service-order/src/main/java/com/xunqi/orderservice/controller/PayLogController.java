package com.xunqi.orderservice.controller;


import com.xunqi.commonutils.R;
import com.xunqi.orderservice.service.PayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
@RestController
@RequestMapping("/orderservice/paylog")
public class PayLogController {

    @Autowired
    private PayLogService payLogService;

    /**
     * 根据订单号生成微信支付二维码接口
     * @param orderNo
     * @return
     */
    @GetMapping(value = "/createNative/{orderNo}")
    public R createNative(@PathVariable("orderNo") String orderNo) {

        //返回信息，包含二维码地址，还有其他信息
        Map map =  payLogService.createNative(orderNo);
        System.out.println("******返回微信二维码的map集合:" + map);

        return R.ok().data(map);
    }

    //查询订单支付状态
    //参数：订单号，根据订单号查询支付状态
    @GetMapping(value = "/queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable("orderNo") String orderNo) {

        Map<String,String> map = payLogService.queryPayStatus(orderNo);
        System.out.println("******查询订单状态的map集合:" + map);

        if (map == null) {
            return R.error().message("支付出错了");
        }

        //如果返回map里面不为空，通过map获取订单状态
        if (map.get("trade_state").equals("SUCCESS")) {
            //支付成功,添加记录到支付表，更新订单表订单状态
            payLogService.updateOrderStatus(map);
            return R.ok().message("支付成功");
        }

        return R.ok().code(25000).message("支付中");
    }

}

