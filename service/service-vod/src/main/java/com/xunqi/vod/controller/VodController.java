package com.xunqi.vod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.xunqi.commonutils.R;
import com.xunqi.servicebase.exception.GuliException;
import com.xunqi.vod.config.VodConfig;
import com.xunqi.vod.service.VodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-04 18:38
 **/

@Api(value = "阿里云视频上传服务",tags = "阿里云视频上传服务")
@RestController
@RequestMapping(value = "/eduvod/video")
public class VodController {

    @Resource
    private VodService vodService;

    /**
     * 上传视频到阿里云
     * @return
     */
    @ApiOperation(value = "上传视频到阿里云",notes = "上传视频到阿里云",httpMethod = "POST")
    @RequestMapping(value = "/uploadAliVideo")
    public R uploadAliVideo(@ApiParam(name = "file",value = "上传文件",required = true)
                                        MultipartFile file) throws IOException {

        //返回上传视频的id值
        String videoId = vodService.uploadAliFile(file);

        return R.ok().data("videoId",videoId);
    }


    /**
     * 根据视频id删除阿里云视频
     */
    @ApiOperation(value = "根据视频id删除阿里云视频",notes = "根据视频id删除阿里云视频",httpMethod = "DELETE")
    @DeleteMapping(value = "/removeAliyVideo/{id}")
    public R removeAliyVideo(@ApiParam(name = "id",value = "视频id",required = true)
                             @PathVariable("id") String id) {

        vodService.removeAliyVideo(id);

        return R.ok();
    }


    //删除多个阿里云视频的方法
    //参数是多个视频的方法
    @DeleteMapping(value = "/delete-batch")
    @ApiOperation(value = "批量删除阿里云视频",notes = "批量删除阿里云视频",httpMethod = "DELETE")
    public R deleteBatch(@ApiParam(name = "videoIdList",value = "视频id",required = true)
                         @RequestParam List<String> videoIdList) {

        vodService.removeMoreAliVideo(videoIdList);

        return R.ok();
    }

    /**
     * 根据视频id获取视频凭证
     * @param id
     * @return
     */
    @GetMapping(value = "/getPlayAuth/{id}")
    public R getPlayAuth(@PathVariable("id") String id) {
        System.out.println(id);
        if ("null".equals(id)) {
            return R.ok().data("playAuth",null);
        }

        //获取视频播放凭证
        DefaultAcsClient client = VodConfig.initVodClient("LTAI4GDKuJeGsyXqwLh16gYn", "Yye0jN1m7O81zE0LHVosBz8d8ZWQer");

        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();
        String playAuth = null;
        try {
            response = getVideoPlayAuth(client,id);
            playAuth = response.getPlayAuth();
            //播放凭证
            System.out.print("PlayAuth = " + playAuth + "\n");
            //VideoMeta信息
            System.out.print("VideoMeta.Title = " + response.getVideoMeta().getTitle() + "\n");
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            throw new GuliException(20001,"获取凭证失败");
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");


        return R.ok().data("playAuth",playAuth);
    }

    /*获取播放凭证函数*/
    public static GetVideoPlayAuthResponse getVideoPlayAuth(DefaultAcsClient client,String id) throws Exception {
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId(id);
        return client.getAcsResponse(request);
    }

}
