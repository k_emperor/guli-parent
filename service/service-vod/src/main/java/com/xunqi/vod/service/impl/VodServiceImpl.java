package com.xunqi.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.xunqi.servicebase.exception.GuliException;
import com.xunqi.vod.config.VodConfig;
import com.xunqi.vod.feign.EduFeign;
import com.xunqi.vod.service.VodService;
import com.xunqi.vod.util.ConstantVodPropertiesUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-04 18:39
 **/

@Service("vodService")
public class VodServiceImpl implements VodService {

    @Autowired
    private ConstantVodPropertiesUtils utils;

    @Autowired
    private EduFeign eduFeign;

    @Override
    public String uploadAliFile(MultipartFile file) throws IOException {

        /**
         * title：上传之后显示名称
         * filename：上传文件原始名称
         * inputStream：上传文件输入流
         */

        InputStream inputStream = file.getInputStream();
        String fileName = file.getOriginalFilename();

        String title = fileName.substring(0,fileName.lastIndexOf("."));

        UploadStreamRequest request = new UploadStreamRequest(utils.getKeyid(), utils.getKeysecret(), title, fileName, inputStream);

        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadStreamResponse response = uploader.uploadStream(request);
        System.out.print("RequestId=" + response.getRequestId() + "\n");  //请求视频点播服务的请求ID

        String videoId = response.getVideoId();

        if (response.isSuccess()) {
            System.out.print("VideoId=" + response.getVideoId() + "\n");
        } else { //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
            System.out.print("VideoId=" + videoId + "\n");
            System.out.print("ErrorCode=" + response.getCode() + "\n");
            System.out.print("ErrorMessage=" + response.getMessage() + "\n");
        }

        return videoId;
    }

    /**
     * 删除视频
     * @param client 发送请求客户端
     * @param id    视频id
     * @return DeleteVideoResponse 删除视频响应数据
     * @throws Exception
     */
    public static DeleteVideoResponse deleteVideo(DefaultAcsClient client,String id) throws Exception {
        DeleteVideoRequest request = new DeleteVideoRequest();
        //支持传入多个视频ID，多个用逗号分隔
        request.setVideoIds(id);
        return client.getAcsResponse(request);
    }

    @Override
    public void removeAliyVideo(String id) {
        DefaultAcsClient client = VodConfig.initVodClient(utils.getKeyid(),utils.getKeysecret());

        DeleteVideoResponse response = new DeleteVideoResponse();
        try {
            response = deleteVideo(client,id);
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            throw new GuliException(20001,"删除视频失败");
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");
    }


    @Override
    public void removeMoreAliVideo(List<String> videoIdList) {

        DefaultAcsClient client = VodConfig.initVodClient(utils.getKeyid(),utils.getKeysecret());

        DeleteVideoResponse response = new DeleteVideoResponse();
        try {

            //将videoIdList转换成1,2,3
            String videoIds = StringUtils.join(videoIdList.toArray(), ",");

            response = deleteVideo(client,videoIds);
        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
            throw new GuliException(20001,"删除视频失败");
        }
        System.out.print("RequestId = " + response.getRequestId() + "\n");

    }

}
